# Change Log
All notable changes to this project will be documented in this file.

## [0.0.4] - 2016-10-28
### Added
- Dynamic Page system
- Vote system
- Propper comments in all model & module files
### Changed
- cleaned up not used code (from 0.0.1 ver.).

## [0.0.3] - 2016-10-17
### Added
- PDO implementation library instead of Procedural mysqli.
- Dynamic vote page Framework.

## [0.0.2] - 2016-10-16
### Fixed
- Fixed memory leak in UCP with database fetch
- Fixed all view pages to be based on index / main page
### Added
- Added basic UCP panel.
### Changed
- Useless code / non used variables clean up.

## 0.0.1 - 2016-10-16
### Added
- The stable release with basic website functions added.

