<?php

class customPageModuleModel extends Provider{

  public $customPageData;

  /*
  // constcruct extended class
  */

  function __construct(){
    parent::__construct();
  }

  /*
  // Clean things allow only letter a-z and numbers
  */

  function cleanData($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $string = str_replace(' ', '-', $data); // Replaces all spaces with hyphens.
    return preg_replace('/[^\-\A-Za-z0-9\-]/', '', $string); // Removes special chars.
  }

  /*
  // Fetch custom page data from database
  */

  function fetchCustomPageData($page){
    $stmt = "SELECT * FROM custom_pages WHERE url_key = ?";
    $params = array($this->cleanData($page));
    $result = $this->simpleFetch("cms",$stmt,$params);
    $this->customPageData['id'] = $result['id'];
    $this->customPageData['title'] = $result['title'];
    $this->customPageData['url_key'] = $result['url_key'];

    // lets parse content global variables
    $content = $result['content'];
    $pattern = array("/CUSTOM_PAGE_TITLE/","/CUSTOM_PAGE_KEY/","/SERVER_HOST/");
    $replacement = array($this->customPageData['title'],$this->customPageData['url_key'],$_SERVER['HTTP_HOST']);
    $parsed = preg_replace($pattern, $replacement, $content);
    $this->customPageData['content'] = $parsed;
  }

  /*
  // prepare array before fetch.
  */

  function prepareData(){
    $this->customPageData = array(
      'id'=>"",'title'=>"",'url_key'=>"",'keywords'=>"",'meta'=>"",'content'=>"",
    );
  }


}
