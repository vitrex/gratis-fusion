<?php

class loginModuleModel extends Provider{

  /*
  // constcruct extended class
  */

  function __construct(){
    parent::__construct();
  }

  /*
  // get and define data
  */

  function getData($data){
    $this->data = $data;
  }

  /*
  // Prepare array pattern
  */

  function preparePattern(){
    $this->patternData = array(`username` => "", `password` => "","session"=> "");
  }

  /*
  // Assign data to prepare pattern array
  */

  function assignData($data){
    $this->patternData['username'] = $data['username'];
    $this->patternData['password'] = SHA1(strtoupper($data['username']). ":" . strtoupper($data['password']));
    $stmt = "SELECT id FROM account WHERE username = '".strtoupper($data['username'])."'";
    $this->patternData['session'] = $this->singleQueryFetch("auth",$stmt,"id");
  }

  /*
  // Trigger login
  */

  function trigerLogin(){
    $this->preparePattern();
    $this->assignData($this->data);
  }

  /*
  // Check if username exists
  */

  function checkUsername($username){
    $stmt = "SELECT username FROM account WHERE username = ?";
    $params = array(strtoupper($username));
    $result = $this->countRows("auth",$stmt,$params);
    if($result > 0){
      return true;
    } else {
      return false;
    }
  }

  /*
  // Check if password for entered username is correct
  */

  function checkPassword($username,$psw){
    $password = SHA1(strtoupper($username). ":" . strtoupper($psw));
    $stmt = "SELECT sha_pass_hash FROM account WHERE sha_pass_hash = ?";
    $params = array($password);
    $result = $this->countRows("auth",$stmt,$params);
    if($result > 0){
      return true;
    } else {
      return false;
    }
  }

}
