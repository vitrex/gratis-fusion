<?php

Class navigationModuleModel extends Provider {

  protected $itemsLimit;
  public $navData;

  /*
  // constcruct extended class
  */

  function __construct(){
    parent::__construct();
    $stmt = "SELECT * FROM nav WHERE id = ?";
    $params = array(1);
    $result = $this->countColumns("cms",$stmt,$params);
    $this->itemsLimit = $result / 2 -1; // % 2 because 1 for title second for url -1 cause of ID column
  }

  /*
  // fetch navigation data
  */

  function fetchNavData($item){
    $stmt = "SELECT * FROM nav WHERE id = ?";
    $params = array(1);
    $result = $this->simpleFetch("cms",$stmt,$params);
    $this->navData[$item]['title'] = $result['nav_item_'.$item];
    $this->navData[$item]['url'] = $result['nav_item_link_'.$item];;
  }

  /*
  // prepare navigation array
  */

  function prepareNavData(){
    $i = 0;
    while($i++ < $this->itemsLimit){
      $this->navData = array(
        $i => array('title' => "", 'url' => "#"),
      );
    }

  }

  /*
  // render all the data
  */

  function renderNavData($item){
    $this->prepareNavData();
    $this->fetchNavData($item);
  }

}
