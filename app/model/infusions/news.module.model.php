<?php

Class newsModuleModel extends Provider {

  public $newsData;
  protected $newsCount;

  /*
  // constcruct extended class
  */

  function __construct(){
    parent::__construct();
    $stmt = "SELECT id FROM news WHERE id > ?";
    $params = array("0");
    $this->newsCount = $this->countRows("cms",$stmt,$params);
  }

  /*
  // fetch news data
  */

  function fetchNewsData($id){
    $stmt = "SELECT * FROM news WHERE id = ?";
    $params = array($id);
    $result = $this->simpleFetch("cms",$stmt,$params);
    $this->newsData[$id]['title'] = $result['title'];
    $this->newsData[$id]['content'] = $result['content'];
    $this->newsData[$id]['date'] = $result['date'];
    $this->newsData[$id]['img'] = $result['img'];
  }

  /*
  // Prepare news data pattern array
  */

  function prepareNewsData(){
    $this->newsdata = array(
      'id' => array(
        'title' => "",'img' => "",'content' => "",'date' => "",
      ),
    );
  }

  /*
  // Render and assign data.
  */

  function renderNewsData($id){
    $this->prepareNewsData();
    $this->fetchNewsData($id);
  }

}
