<?php

Class registerModuleModel extends Provider {

  protected $data;
  protected $patternData;

  /*
  // constcruct extended class
  */

  function __construct(){
    parent::__construct();
  }

  /*
  // get and assign data
  */

  function getData($data){
    $this->data = $data;
  }

  /*
  // prepare pattern array
  */

  function preparePattern(){
    $this->patternData = array(
      'id' => NULL,`username` => "", `sha_pass_hash` => "", `email` => "",'expansion' => "2",
    );
  }

  /*
  // assign data to patterna array
  */

  function assignData($data){
    $this->patternData['username'] = strtoupper($data['username']);
    $this->patternData['sha_pass_hash'] = SHA1(strtoupper($data['username']). ":" . strtoupper($data['password']));
    $this->patternData['email'] = $data['email'];
  }

  /*
  // Triger register
  */

  function trigerRegister(){
    $this->preparePattern();
    $this->assignData($this->data);
    $stmt = "INSERT INTO `account` (`username`, `sha_pass_hash`, `email`, `expansion`) VALUES (?,?,?,?)";
    $this->connect("auth");
    $stmt = $this->conn->prepare($stmt);
    $stmt->bindValue(1,$this->patternData['username'], PDO::PARAM_STR);
    $stmt->bindValue(2,$this->patternData['sha_pass_hash'], PDO::PARAM_STR);
    $stmt->bindValue(3,$this->patternData['email'], PDO::PARAM_STR);
    $stmt->bindValue(4,$this->patternData['expansion'], PDO::PARAM_INT);
    $stmt->execute();
  }

  /*
  // Check if entered username exists
  */

  function checkUsername($username){
    $stmt = "SELECT username FROM account WHERE username = ?";
    $params = array(strtoupper($username));
    $result = $this->countRows("auth",$stmt,$params);
    if($result > 0){
      return true;
    } else {
      return false;
    }
  }

  /*
  // Check if entered email exists
  */

  function checkEmail($email){
    $stmt = "SELECT username FROM account WHERE username = ?";
    $params = array($email);
    $result = $this->countRows("auth",$stmt,$params);
    if($result > 0){
      return true;
    } else {
      return false;
    }
  }

}
