<?php

Class serverStatusModule extends Provider {

  public $serverData;

  /*
  // constcruct extended class
  */

  function __construct(){
    parent::__construct();
  }

  /*
  // Fetch realm data
  */

  function fetchRealmData($id){
    // params
    $params = array($id);
    // fetch cms realms data
    $cms_stmt = "SELECT * FROM realms WHERE id = ?";
    $cms_result = $this->simpleFetch("cms",$cms_stmt,$params);
    // fetch auth realmlist data
    $auth_stmt = "SELECT * FROM realmlist WHERE id = ?";
    $auth_result = $this->simpleFetch("auth",$auth_stmt,$params);
    // online players count
    $char_params = array(1);
    $char_stmt = "SELECT * FROM characters WHERE online = ?";
    $char_result = $this->countRows("char",$char_stmt,$params);
    // Define shit here
    $this->serverData[$id]['name'] = $auth_result["name"];
    $this->serverData[$id]['expansion'] = $cms_result["expansion"];
    $this->serverData[$id]['ip'] = $auth_result["address"];
    $this->serverData[$id]['port'] = $auth_result["port"];
    $this->serverData[$id]['onlinePlayers'] = $char_result;
    $this->serverData[$id]['onlinePlayersLimit'] = $cms_result["limit"];
  }

  /*
  // prepare pattern array
  */

  function prepareServerData($id){
    $this->serverData = array(
      $id = array(
      "expansion" => "","name" => "","ip" => "","port" => "",
      "status" => "","onlinePlayers" => "","onlinePlayersLimit" => "",
      "expasion_icon"=>"","expansion_name"=>""
      ),
    );
  }

  /*
  // check server status
  */

  function getServerStatus($id){
   $connect = @fsockopen($this->serverData[$id]['ip'], $this->serverData[$id]['port'], $errno, $errstr, 0.2);
     if($connect){
       $this->serverData[$id]['status'] = true;
     } else{
       $this->serverData[$id]['status'] = false;
     }
   @fclose($connect);
  }

  /*
  // Get server expansion details
  */

  function getServerExpansionDetails($id){
      switch($this->serverData[$id]['expansion']) {
        case "0" :
        $this->serverData[$id]['expansion_name'] = "Vanila";
        $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/vanila.png'
          alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        break;
        case "1" :
          $this->serverData[$id]['expansion_name'] = "Burning Crusade";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/bc.gif'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "2" :
          $this->serverData[$id]['expansion_name'] = "Wrath Of The Lich King";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/wotlk.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "3" :
          $this->serverData[$id]['expansion_name'] = "Cataclysm";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/cata.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "4" :
          $this->serverData[$id]['expansion_name'] = "Mist Of Pandaria";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/mop.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "5" :
          $this->serverData[$id]['expansion_name'] = "Warlords Of Draenor";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/warlords.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
        case "6" :
          $this->serverData[$id]['expansion_name'] = "Legion";
          $this->serverData[$id]['expansion_icon'] = "<img src = 'assets/components/expansion_icons/legion.png'
            alt = '".$this->serverData[$id]['expansion_name']."' title = '".$this->serverData[$id]['expansion_name']."'/>";
        Break;
    }
  }

  /*
  // Render realm data
  */

  function renderRealmData($id){
      // define all stuff
      $this->prepareServerData($id);
      $this->fetchRealmData($id);
      $this->getServerStatus($id);
      $this->getServerExpansionDetails($id);
  }
}
