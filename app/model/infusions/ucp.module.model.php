<?php

class ucpModuleModel extends Provider {

  public $accountDataPattern;
  public $getCharactersQuery;
  public $getVoteQuery;

  /*
  // constcruct extended class
  */

  function __construct($id){
    parent::__construct();
    $stmt = "SELECT name FROM characters WHERE account = ?";
    $params = array($id);
    $this->getCharactersQuery = $this->insertQuery("char",$stmt,$params);

    $stmt_vote = "SELECT * FROM vote_table WHERE id > 0";
    $params = array(0);
    $this->getVoteQuery = $this->simpleQuery("cms",$stmt_vote);
  }

  /*
  // Clean things and allow only numbers 0-9
  */

  function cleanData($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $string = str_replace(' ', '-', $data); // Replaces all spaces with hyphens.
    return preg_replace('/0-9/', '', $string); // Removes special chars.
  }

  function preparePattern(){
    $this->accountDataPattern = array("");
  }

  /*
  // Account data
  */

  function userVoted($user,$voteID){
    $stmt_vote = "SELECT * FROM vote_log WHERE vote_id = ?";
    $params = array($voteID);
    $result = $this->simpleFetch("cms",$stmt_vote,$params);
    $count = $this->countRows("cms",$stmt_vote,$params);
    if($count > 0){
      if($result['timestamp'] <= time()){
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  function assignAccountData($id){
    $stmt = "SELECT * FROM account WHERE id = ?";
    $params = array($id);
    $query = $this->simpleFetch("auth",$stmt,$params);
    $this->accountDataPattern = $query;
  }

  function renderAccountInfo($id,$value){
    $this->preparePattern();
    $this->assignAccountData($id);
    return $this->accountDataPattern[$value];
  }

  /*
  // Character data
  */

  function setCharsAmmount($account){
    $stmt = "SELECT * FROM characters WHERE account = ?";
    $params = array($account);
    $query = $this->insertQuery("char",$stmt,$params);
    return mysqli_num_rows($query);
  }

  /*
  // Vote data
  */

}
