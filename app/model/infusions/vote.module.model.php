<?php
  class voteModuleModel extends Provider{

    public $getVoteQuery;

    /*
    // constcruct extended class
    */

    function __construct(){
      parent::__construct();
    }

    /*
    // Check if user voted.
    */

    function userVoted($user,$voteID){
      $stmt_vote = "SELECT * FROM vote_log WHERE vote_id = ?";
      $params = array($voteID);
      $result = $this->simpleFetch("cms",$stmt_vote,$params);
      $count = $this->countRows("cms",$stmt_vote,$params);
      if($count > 0){
        if($result['timestamp'] <= time()){
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    }

    /*
    // fetch vote page data from database
    */

    function getVoteData($page){
      $stmt = "SELECT * FROM vote_table WHERE id = ?";
      $params = array($page);
      return $this->getVoteQuery = $this->simpleFetch("cms",$stmt,$params);
    }

    /*
    // Insert vote & update user vote points
    */

    function submitVote($user,$voteID,$time,$voteReward){
      $stmt_vote = "SELECT * FROM vote_log WHERE vote_id = ?";
      $params = array($voteID);
      $count = $this->countRows("cms",$stmt_vote,$params);

      /*
      // if user already voted before lets update row
      */

      if($count > 0){
        $stmt = "UPDATE `vote_log` SET `vote_id`= ? ,`user_id`=?, `timestamp`= ? WHERE vote_id = ?";
        $this->connect("cms");
        $stmt = $this->conn->prepare($stmt);
        $stmt->bindValue(1,$voteID, PDO::PARAM_INT);
        $stmt->bindValue(2,$user, PDO::PARAM_INT);
        $stmt->bindValue(3,$time, PDO::PARAM_STR);
        $stmt->bindValue(4,$voteID, PDO::PARAM_INT);
        $stmt->execute();

        $stmt2 = "UPDATE `account` SET `vote_points`= vote_points + ? WHERE id = ?";
        $this->connect("auth");
        $stmt2 = $this->conn->prepare($stmt2);
        $stmt2->bindValue(1,$voteReward, PDO::PARAM_INT);
        $stmt2->bindValue(2,$user, PDO::PARAM_INT);
        $stmt2->execute();

      } else {

        /*
        // else (user never voted before) insert new row into log
        */

        $stmt = "INSERT INTO `vote_log` (`vote_id`, `user_id`, `timestamp`)
        VALUES (?, ?, ?)";
        $this->connect("cms");
        $stmt = $this->conn->prepare($stmt);
        $stmt->bindValue(1,$voteID, PDO::PARAM_INT);
        $stmt->bindValue(2,$user, PDO::PARAM_INT);
        $stmt->bindValue(3,$time, PDO::PARAM_STR);
        $stmt->execute();

        $stmt2 = "UPDATE `account` SET `vote_points`= vote_points + ? WHERE id = ?";
        $this->connect("auth");
        $stmt2 = $this->conn->prepare($stmt2);
        $stmt2->bindValue(1,$voteReward, PDO::PARAM_INT);
        $stmt2->bindValue(2,$user, PDO::PARAM_INT);
        $stmt2->execute();
      }
    }

  }
