<?php

class mainPageModel extends Provider {

  /*
   Main variable used everywhere to acces web setings
  */
  public $webData;

  /*
   url variable defined here
  */
  protected $url;

  /*
   Basicly constructing parent class (mysqlLib)
  */
  function __construct(){
    parent::__construct();
    /*
     Lets define everything we need here to have in our webdata variable
    */
    $stmt = "SELECT * FROM web_settings WHERE id > ?";
    $params = array("0");
    $query = $this->simpleFetch("cms",$stmt,$params);
    $this->webData = $query;
  }

  function cleanData($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $string = str_replace(' ', '-', $data); // Replaces all spaces with hyphens.
    return preg_replace('/[^\-\A-Za-z\-]/', '', $string); // Removes special chars.
  }

  function checkPageExistDatabase($page){
    $stmt = "SELECT * FROM custom_pages WHERE url_key = ?";
    $params = array($page);
    $count = $this->countRows("cms",$stmt,$params);
    return $count;
  }

  /*
   renderView - renders view of the including all essentials for it to work.
  */

  function renderview($url){

    $this->url = $this->cleanData($url);

    if($this->checkPageExistDatabase($this->url) > 0){
      /*
       Render Theme / Template files
      */
      require_once THEME_PATH.$this->webData["template"]."/template.php";
      /*
       Modules Infusions here
      */
      require_once INFUSIONS_PATH."settings.php";
      foreach($settings as $key){
        if($key['status'] == "enable") {
          if($key['model_path'] != "") {
            include INFUSION_MODEL_PATH.$key['model_path'];
          }
          include INFUSIONS_PATH.$key['path'];
        }
      }
      /*
       Render the actual view output.
      */
      include VIEW_PATH.'includes/header.phtml';
        include VIEW_PATH.'custom.phtml';
      include VIEW_PATH.'includes/footer.phtml';
    } else {
      if(file_exists(VIEW_PATH.$this->url.".phtml")) {
          /*
           Render Theme / Template files
          */
          require_once THEME_PATH.$this->webData["template"]."/template.php";
          /*
           Modules Infusions here
          */
          require_once INFUSIONS_PATH."settings.php";
          foreach($settings as $key){
            if($key['status'] == "enable") {
              if($key['model_path'] != "") {
                include INFUSION_MODEL_PATH.$key['model_path'];
              }
              include INFUSIONS_PATH.$key['path'];
            }
          }
          /*
           Render the actual view output.
          */
          include VIEW_PATH.'includes/header.phtml';
            include VIEW_PATH.$this->url.'.phtml';
          include VIEW_PATH.'includes/footer.phtml';
        } else {
          /*
           Throw 404 if page doesn't exist
          */
          die("404");
        }
      }
    }

}
