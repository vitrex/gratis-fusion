<?php
class template extends Provider {

    function openPanel($title){
      echo '<div class="panel panel-default">';
        echo '<div class="panel-heading panel-default">'.$title.'</div>';
        echo '<div class="panel-body">';
    }

    function closePanel(){
        echo '</div>';
      echo '</div>';
    }

    function headerTop($title,$caption){
      echo '<div id="myCarousel" class="carousel slide" data-ride="carousel">';
          echo '<div class="carousel-inner" role="listbox">';
            echo '<div class="item slide_2 active">';
              echo '<div class="carousel-caption">';
                echo '<h3>'.$title.'</h3>';
                echo '<p>'.$caption.'</p>';
              echo '</div>';
            echo '</div>';
          echo '</div>';
        echo '</div>';
    }

    function slider($img1,$img2,$img3){
      echo '<div id="SlideShow" class="carousel slide" data-ride="carousel">';
      echo '<ul class="nav nav-pills nav-justified">';
        echo '<li data-target="#SlideShow" data-slide-to="0"><a href="#">Slide 1</a></li>';
        echo '<li data-target="#SlideShow" data-slide-to="1"><a href="#">Slide 2</a></li>';
        echo '<li data-target="#SlideShow" data-slide-to="2"><a href="#">Slide 3</a></li>';
      echo '</ul>';
            echo '<div class="carousel-inner" role="listbox">';
              echo '<div class="item active '.$img1.'"></div>';
              echo '<div class="item '.$img2.'"></div>';
              echo '<div class="item '.$img3.'"></div>';
            echo '</div>';
      echo '</div>';
    }

    function ucpMenu(){
      ?>
      <ul class="nav nav-pills">
        <li role="presentation"><a href="index.php?p=ucp">SUMMARY</a></li>
        <li role="presentation"><a href="index.php?p=ucp&sub=vote">VOTE</a></li>
        <li role="presentation"><a href="index.php?p=ucp&sub=donate">DONATE</a></li>
        <li role="presentation"><a href="index.php?p=ucp&sub=store">STORE</a></li>
        <li role="presentation"><a href="index.php?p=ucp&sub=services">SERVICES</a></li>
        <li role="presentation"><a href="index.php?p=ucp&sub=security">SECURITY</a></li>
        <li role="presentation"><a href="index.php?p=ucp&sub=settings">SETTINGS</a></li>
      </ul>
      <hr />
      <?
    }

    function ucpSideMenu($username,$vp,$dp){
      ?>
      <p>Welcome back, <strong><? echo strtolower($username); ?></strong> !</p>
      <p>VP <? echo $vp; ?></p>
      <p>DP <? echo $dp; ?></p>
      <hr />
      <a href = "index.php?p=ucp&action=logout" class = "text-danger">Logout</a>
      <?
    }


}

$tpl = new template();
