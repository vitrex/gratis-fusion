<?php
// Report all PHP errors (see changelog)
error_reporting(E_ALL);

session_start();
require_once 'app/boot.php'; // call the boot file where we define everything we need

class page extends mainPageModel {

  /*
  // constcruct extended class
  */

  function __construct(){
    parent::__construct();
  }

  // define load page function and render view of the page
  function loadPage($url){
    parent::renderview($url);
  }

}

$url = isset($_GET['p']) ? $_GET['p'] : "index";
$page = new page();
$page->loadPage($url);
