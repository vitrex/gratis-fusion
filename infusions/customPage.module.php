<?php

class customPage extends customPageModuleModel {

  /*
  // constcruct extended class
  */
  function __construct(){
    parent::__construct();
  }

  /*
  // Render custom content
  */

  function renderCustomContent($page,$value){
    parent::fetchCustomPageData($page);
    return $this->customPageData[$value];
  }
}

$m_customPage = new customPage();
