<?php

  class login extends loginModuleModel{

    public $result;
    public $loginData;

    /*
    // constcruct extended class
    */

    function __construct(){
      parent::__construct();
    }

    /*
    // Clean things up allow only letter a-z and numbers 0-9
    */

    function cleanData($data){
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			$string = str_replace(' ', '-', $data); // Replaces all spaces with hyphens.
			return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /*
    // Validate and sanitize inputs
    */

    function validateInputs($data){
      $this->loginData = array(
        "username" => $this->cleanData($data["username"]),
        "password" => $this->cleanData($data["password"]),
      );

      if(!$this->checkUsername($this->loginData['username'])){
        $this->result = "<p>There is no account with this username.</p>";
        return false;
      }

      if(!$this->checkPassword($this->loginData['username'],$this->loginData['password'])){
        $this->result = "<p>Incorrect password.</p>";
        return false;
      }

      return true;
    }

    /*
    // Triger submit button
    */

    function trigerSubmit($data){
      if($this->validateInputs($data)){
        parent::getData($this->loginData); // pass data to model
        // login if everything is ok.
          $this->trigerLogin();
          $_SESSION['user'] = $this->patternData['session'];
          header("location: index.php?p=ucp");
          exit();
        }
      }
  }

$m_login = new login();
