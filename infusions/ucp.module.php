<?php

class ucp extends ucpModuleModel {

  protected $id;
  public $totalChars;

  /*
  // constcruct extended class
  */

  function __construct($id){
    $this->setId($id);
    parent::__construct($this->id);
  }

  /*
  // get user account data from parent class
  */

  function getUserAccountData($value){
    return parent::renderAccountInfo($this->id,$value);
  }

  /*
  // get users char data from parent class
  */

  function getUserCharData($char,$value){
    $this->charID = parent::setCharacerID($account);
    return parent::renderCharInfo($this->id,$value,$this->charID);
  }

  /*
  // fix last seen status in UCP
  */

  function convertlastSeenToDays($time){
    if($time == "0000-00-00 00:00:00") {
      return "Never been online";
    } else {
    $time = time() - strtotime($time);
    $time /= 60 * 60 * 24;
    return round($time)." Days ago";
    }
  }

  /*
  // set the ID to global variable
  */

  function setId($id){
    $this->id = $id;
  }

  /*
  // render services (not finished don't know what services to offer...)
  */

  function renderServicesForm(){
    ?>
    <form method="post" id="services" name = "services" autocomplete="off">
      <div class="form-group">
        <select class="form-control" id="char" name = "char">
          <option value = "">Select Character</option>
          <?
            $query = $this->getCharactersQuery;
            while($row = mysqli_fetch_array($query)){
              ?>
               <option value = "<? echo $row['guid']; ?>"><? echo $row['name']; ?></option>
              <?
            }
          ?>
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" id="char" name = "char">
          <option value = "">Select Service</option>
        </select>
      </div>
      <button type="submit" name = "submit" class="btn btn-success btn-md btn-block">Submit</button>
      <br />
    </form>
    <?
  }

  /*
  // Render vote front page
  */

  function renderVotePage(){
    echo '<div class= "row">';
      foreach($this->getVoteQuery as $key){
        ?>
        <div class = "col-md-2 text-center">
          <strong><? echo $key['title']; ?></strong> <hr />
          <div class = "vote">
          <?
          if(!parent::userVoted($this->id,$key['id'])):
          ?>
           <a href = "index.php?p=vote&page=<? echo $this->cleanData($key['id'])?>" target="_blank">
             <img src = "<? echo $key['image'] ?>"/>
           </a>
          <? else: ?>
             <img src = "<? echo $key['image'] ?>" style = "opacity:0.5;"/>
          <? endif; ?>
           <p><strong>Reward : <? echo $key['points']; ?></strong></p>
           <hr />
          </div>
        </div>
        <?
      }
    echo '</div>';
  }

}

$m_ucp = @new ucp($_SESSION['user']);
