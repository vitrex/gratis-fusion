<?php
  class vote extends voteModuleModel{

    public $userVoted;

    /*
    // Clean things and allow only numbers
    */

    function cleanData($data){
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      $string = str_replace(' ', '-', $data); // Replaces all spaces with hyphens.
      return preg_replace('/0-9/', '', $string); // Removes special chars.
    }

    /*
    // constcruct extended class
    */

    function __construct(){
      parent::__construct();
    }

    /*
    // Render Vote status returning value from model
    */

    function renderVoteStatus($user,$page){
      return parent::userVoted($user,$page);
    }

    /*
    // Render Vote data returning value from model
    */
    function renderVoteData($page){
      return parent::getVoteData($page);
    }

    /*
    // Triggering submit vote function adding values to it.
    */

    function renderVoteSubmit($user,$page){
      $voteData = $this->renderVoteData($page);
      $time = time() + 60 * 60 * 12;
      parent::submitVote($user,$page,$time,$voteData['points']);
    }

  }

/*
// Construct class so we can use it in view files.
*/

$m_vote = new vote();
