<?php

class Provider {

  // define variables
  public $conn;
  protected $conf;
  protected $list;

  function __construct(){
    // require config file and define $this->conf to array
    include CONFIGS_PATH.'db_conf.php';
    $this->conf = $confData['database'];
    $this->list = array(
      "world" => $this->conf['worldDB'],
      "char" => $this->conf['charDB'],
      "auth" => $this->conf['authDB'],
      "cms" => $this->conf['webDB'],
    );
  }

  function connect($type){
    /*
    // Create connection here
    */
    try
     {
         $this->conn = new PDO("mysql:host=".$this->conf['host'].";dbname=".$this->list[$type]."",$this->conf['user'],$this->conf['password']);
         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     }
     catch(PDOException $e)
     {
         echo "ERROR : ".$e->getMessage();
     }
    // return connection variable for later use
    return $this->conn;
  }

  /*
  // Single value fetch with selected value
  */
  function singleQueryFetch($type,$query,$value,$params){
    $this->connect($type);
    $stmt = $this->conn->prepare($query);
    $stmt->execute($params);
    $fetch = $stmt->FETCH(PDO::FETCH_ASSOC);
    return $fetch[$value];
  }

  /*
  // Simple query to fetch stuff. :)
  */
  function simpleQuery($type,$query){
    $this->connect($type);
    $stmt = $this->conn->query($query);
    return $stmt;
  }


  /*
  // Full row fetch
  */
  function simpleFetch($type,$query,$params){
    $this->connect($type);
    $stmt = $this->conn->prepare($query);
    $stmt->execute($params);
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  /*
  // Single line query insert (not used to insert data to database only select).
  */
  function insertQuery($type,$query,$params){
    $this->connect($type);
    $stmt = $this->conn->prepare($query);
    return $stmt->execute($params);
  }

  /*
  // Count rows
  */
  function countRows($type,$query,$params){
    $this->connect($type);
    $stmt = $this->conn->prepare($query);
    $stmt->execute($params);
    return $stmt->rowCount();
  }

  /*
  // Count columns
  */
  function countColumns($type,$query,$params){
    $this->connect($type);
    $stmt = $this->conn->prepare($query);
    $stmt->execute($params);
    return $stmt->columnCount();
  }



}
