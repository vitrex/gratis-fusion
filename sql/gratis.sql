-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.31-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.5111
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table gratis.nav
CREATE TABLE IF NOT EXISTS `nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_item_1` varchar(50) DEFAULT NULL,
  `nav_item_2` varchar(50) DEFAULT NULL,
  `nav_item_3` varchar(50) DEFAULT NULL,
  `nav_item_4` varchar(50) DEFAULT NULL,
  `nav_item_5` varchar(50) DEFAULT NULL,
  `nav_item_6` varchar(50) DEFAULT NULL,
  `nav_item_7` varchar(50) DEFAULT NULL,
  `nav_item_8` varchar(50) DEFAULT NULL,
  `nav_item_9` varchar(50) DEFAULT NULL,
  `nav_item_10` varchar(50) DEFAULT NULL,
  `nav_item_11` varchar(50) DEFAULT NULL,
  `nav_item_12` varchar(50) DEFAULT NULL,
  `nav_item_link_1` varchar(50) DEFAULT NULL,
  `nav_item_link_2` varchar(50) DEFAULT NULL,
  `nav_item_link_3` varchar(50) DEFAULT NULL,
  `nav_item_link_4` varchar(50) DEFAULT NULL,
  `nav_item_link_5` varchar(50) DEFAULT NULL,
  `nav_item_link_6` varchar(50) DEFAULT NULL,
  `nav_item_link_7` varchar(50) DEFAULT NULL,
  `nav_item_link_8` varchar(50) DEFAULT NULL,
  `nav_item_link_9` varchar(50) DEFAULT NULL,
  `nav_item_link_10` varchar(50) DEFAULT NULL,
  `nav_item_link_11` varchar(50) DEFAULT NULL,
  `nav_item_link_12` varchar(50) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table gratis.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `date` varchar(50) DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gratis.realms
CREATE TABLE IF NOT EXISTS `realms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expansion` int(11) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  KEY `Column 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table gratis.web_settings
CREATE TABLE IF NOT EXISTS `web_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text,
  `keywords` varchar(50) DEFAULT NULL,
  `template` varchar(50) DEFAULT NULL,
  `cache` int(11) DEFAULT NULL,
  `key_1` varchar(50) DEFAULT NULL,
  `realmlist` varchar(50) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
